package com.trophonix.repairit.util;

import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.text.DecimalFormat;

public class EconomyHandler {

  public static final DecimalFormat moneyFormat = new DecimalFormat("###,##0.##");

  private Economy economy;

  public EconomyHandler() {
    economy = Bukkit.getServicesManager().getRegistration(Economy.class).getProvider();
  }

  public double getBalance(Player player) {
    return economy.getBalance(player);
  }

  public String getBalanceString(Player player) {
    double balance = getBalance(player);
    String name = balance == 1 ? economy.currencyNameSingular() : economy.currencyNamePlural();
    return moneyFormat.format(balance) + " " + name;
  }

  public void add(Player player, double amount) {
    economy.depositPlayer(player, amount);
  }

  public void remove(Player player, double amount) {
    economy.withdrawPlayer(player, amount);
  }

}
