package com.trophonix.repairit.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.configuration.ConfigurationSection;

@RequiredArgsConstructor @Getter
public class Config {

  private final ConfigurationSection config;

  private boolean economyEnabled;

  private double pricePerDamagePoint;
  private double reductionFactor;
  private boolean roundPrice;

  private ConfigMessage invalidItem;
  private ConfigMessage invalidNumber;

  private boolean confirmLargeRepair;
  private int largeRepairMinimum;
  private ConfigMessage confirmLargeRepairMessage;
  private ConfigMessage confirmLargeRepairAllMessage;

  private ConfigMessage failureNoRepairableItems;
  private ConfigMessage failureAlreadyRepaired;
  private ConfigMessage failureInsufficientFunds;

  private ConfigMessage repairSuccess;
  private ConfigMessage repairAllSuccess;

  private ConfigMessage free;
  private ConfigMessage reduced;
  private ConfigMessage full;

  public void load() {
    economyEnabled = config.getBoolean("economyEnabled");

    pricePerDamagePoint = config.getDouble("pricePerDamagePoint");
    reductionFactor = config.getDouble("reductionFactor");
    roundPrice = config.getBoolean("roundPrice");

    invalidItem = new ConfigMessage(config, "invalidItem");
    invalidNumber = new ConfigMessage(config, "invalidNumber");

    confirmLargeRepair = config.getBoolean("confirmLargeRepair.enabled");
    largeRepairMinimum = config.getInt("confirmLargeRepair.minimum");
    confirmLargeRepairMessage = new ConfigMessage(config, "confirmLargeRepair.message");
    confirmLargeRepairAllMessage = new ConfigMessage(config, "confirmLargeRepair.allMessage");

    failureNoRepairableItems = new ConfigMessage(config, "failureNoRepairableItems");
    failureAlreadyRepaired = new ConfigMessage(config, "failureAlreadyRepaired");
    failureInsufficientFunds = new ConfigMessage(config, "failureInsufficientFunds");

    repairSuccess = new ConfigMessage(config, "repairSuccess");
    repairAllSuccess = new ConfigMessage(config, "repairAllSuccess");

    free = new ConfigMessage(config, "free");
    reduced = new ConfigMessage(config, "reduced");
    full = new ConfigMessage(config, "full");
  }

}
