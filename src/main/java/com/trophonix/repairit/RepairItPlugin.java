package com.trophonix.repairit;

import com.trophonix.repairit.commands.RepairAllCommand;
import com.trophonix.repairit.commands.RepairCommand;
import com.trophonix.repairit.config.Config;
import com.trophonix.repairit.util.EconomyHandler;
import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class RepairItPlugin extends JavaPlugin {

  private Config conf;
  private EconomyHandler economy;

  @Override public void onEnable() {
    super.getConfig().options().copyDefaults(true);
    saveConfig();

    conf = new Config(getConfig());
    conf.load();

    if (conf.isEconomyEnabled() && getServer().getPluginManager().isPluginEnabled("Vault")) {
      try {
        economy = new EconomyHandler();
      } catch (RuntimeException ignored) {
        getLogger().warning("Failed to hook into Vault. Economy will not function!");
      }
    } else {
      getLogger().info("Failed to find vault. Economy will not function.");
    }

    getCommand("repair").setExecutor(new RepairCommand(this));
    getCommand("repairall").setExecutor(new RepairAllCommand(this));
  }

  @Override public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
    if (command.getName().equals("repairit")) {
      if (args.length > 0 && (args[0].equalsIgnoreCase("rl") || args[0].equalsIgnoreCase("reload"))) {
        reloadConfig();
        conf.load();
        return true;
      }
      sender.sendMessage(ChatColor.AQUA + "/repairit rl");
    }
    return true;
  }
}
